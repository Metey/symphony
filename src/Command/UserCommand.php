<?php

declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use MyBuilder\Bundle\CronosBundle\Annotation\Cron;
use Symfony\Component\Console\Input\InputArgument;
use App\Command\UserManager;


/**
 * @Cron(minute="/10", noLogs=true)
 */
class UserCommand extends Command
{
    public const EMAIL_USER = 'some-user@gmail.com';
    public const PASS_USER = '123456';

    /**
     * @var UserManager
     */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        parent::__construct();
        $this->userManager = $userManager;
    }

    protected function configure()
    {
        $this
            ->setName('user:create')
            ->setDescription('Create a test user.');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->userManager->recordEvent(
            self::EMAIL_USER,
            self::PASS_USER
        );
    }
}