<?php
declare(strict_types=1);

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;

/**
 * Записывает нового пользователя в БД
 */
class UserManager extends AbstractController
{
    private $passwordEncoder;

    /**
     * Конструктор.
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param $email
     * @param $plainPassword
     */
    public function recordEvent($email, $plainPassword)
    {
        $manager = $this->getDoctrine()->getManager();

        $user = new User();
        $user->setEmail($email);
        $user->setPlainPassword($plainPassword);
        $password = $this->passwordEncoder->encodePassword(
            $user,
            $user->getPlainPassword()
        );
        $user->setConfirmationCode('');
        $user->setPassword($password);
        $user->setEnable(true);

        $manager->persist($user);
        $manager->flush();
    }
}