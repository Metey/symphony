<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200213140741 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX IDX_3AF34668AA9E377A ON categories');
        $this->addSql('DROP INDEX IDX_3AF346685E237E06 ON categories');
        $this->addSql('ALTER TABLE comment CHANGE post_id post_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C4B89032C FOREIGN KEY (post_id) REFERENCES posts (id)');
        $this->addSql('DROP INDEX IDX_885DBAFA7B00651C ON posts');
        $this->addSql('DROP INDEX IDX_885DBAFA8B8E8428 ON posts');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE INDEX IDX_3AF34668AA9E377A ON categories (date)');
        $this->addSql('CREATE INDEX IDX_3AF346685E237E06 ON categories (name)');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C4B89032C');
        $this->addSql('ALTER TABLE comment CHANGE post_id post_id INT DEFAULT NULL');
        $this->addSql('CREATE INDEX IDX_885DBAFA7B00651C ON posts (status)');
        $this->addSql('CREATE INDEX IDX_885DBAFA8B8E8428 ON posts (created_at)');
    }
}
