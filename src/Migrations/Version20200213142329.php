<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200213142329 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE categories ADD user_id BIGINT DEFAULT NULL, ADD title VARCHAR(255) DEFAULT NULL, ADD body LONGTEXT DEFAULT NULL, ADD slug VARCHAR(255) DEFAULT NULL, ADD published_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE name status VARCHAR(255) NOT NULL, CHANGE date created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE categories ADD CONSTRAINT FK_3AF34668A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_3AF34668A76ED395 ON categories (user_id)');
        $this->addSql('CREATE INDEX IDX_3AF346687B00651C ON categories (status)');
        $this->addSql('CREATE INDEX IDX_3AF346688B8E8428 ON categories (created_at)');
        $this->addSql('ALTER TABLE posts DROP FOREIGN KEY FK_885DBAFAA76ED395');
        $this->addSql('DROP INDEX IDX_885DBAFA8B8E8428 ON posts');
        $this->addSql('DROP INDEX IDX_885DBAFAA76ED395 ON posts');
        $this->addSql('DROP INDEX IDX_885DBAFA7B00651C ON posts');
        $this->addSql('ALTER TABLE posts ADD date DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', DROP user_id, DROP title, DROP body, DROP slug, DROP created_at, DROP published_at, DROP updated_at, CHANGE status name VARCHAR(255) NOT NULL');
        $this->addSql('CREATE INDEX IDX_885DBAFA5E237E06 ON posts (name)');
        $this->addSql('CREATE INDEX IDX_885DBAFAAA9E377A ON posts (date)');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C4B89032C');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C4B89032C FOREIGN KEY (post_id) REFERENCES categories (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE categories DROP FOREIGN KEY FK_3AF34668A76ED395');
        $this->addSql('DROP INDEX IDX_3AF34668A76ED395 ON categories');
        $this->addSql('DROP INDEX IDX_3AF346687B00651C ON categories');
        $this->addSql('DROP INDEX IDX_3AF346688B8E8428 ON categories');
        $this->addSql('ALTER TABLE categories ADD date DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', DROP user_id, DROP title, DROP body, DROP slug, DROP created_at, DROP published_at, DROP updated_at, CHANGE status name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C4B89032C');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C4B89032C FOREIGN KEY (post_id) REFERENCES posts (id)');
        $this->addSql('DROP INDEX IDX_885DBAFA5E237E06 ON posts');
        $this->addSql('DROP INDEX IDX_885DBAFAAA9E377A ON posts');
        $this->addSql('ALTER TABLE posts ADD user_id BIGINT DEFAULT NULL, ADD title VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD body LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD slug VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD published_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', CHANGE name status VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE date created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE posts ADD CONSTRAINT FK_885DBAFAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_885DBAFA8B8E8428 ON posts (created_at)');
        $this->addSql('CREATE INDEX IDX_885DBAFAA76ED395 ON posts (user_id)');
        $this->addSql('CREATE INDEX IDX_885DBAFA7B00651C ON posts (status)');
    }
}
