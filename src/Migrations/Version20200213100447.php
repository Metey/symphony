<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200213100447 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user ADD client_id VARCHAR(255) DEFAULT NULL, ADD username VARCHAR(255) NOT NULL, ADD oauth_type VARCHAR(255) NOT NULL, ADD last_login DATETIME DEFAULT NULL, CHANGE id id BIGINT AUTO_INCREMENT NOT NULL, CHANGE password password VARCHAR(255) DEFAULT NULL, CHANGE plain_password plain_password VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP client_id, DROP username, DROP oauth_type, DROP last_login, CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE password password VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE plain_password plain_password VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
