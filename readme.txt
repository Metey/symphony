====Symphony====
привязать файл настроек NGINX:
sudo ln -s /www/symphony/config/nginx_local.conf symphony

php bin/console server:run
выполните server:start и продолжайте работать с консолью дальше.
.env настройки подключения к базе
php bin/console doctrine:database:create  создать базу
php bin/console make:controller PostsController
php bin/console make:entity Post  создать сущность
php bin/console make:entity --regenerate генерирует сеттеры-геттеры после добавления полей вручную
php bin/console make:migration создать миграцию
php bin/console doctrine:migrations:diff создать миграцию, сравнив разницу
php bin/console doctrine:migrations:execute --down 20180724141244 Откатить миграию
php bin/console doctrine:migrations:migrate применить миграцию
php bin/console doctrine:fixtures:load запустить наши фикстуры
php bin/console make:form  создать форму
php bin/console cache:clear чистка кэша

composer require --dev doctrine/doctrine-fixtures-bundle скачать DoctrineFixturesBundle
composer require fzaninotto/faker скачаем популярную библиотеку Faker
composer require cocur/slugify библиотека cocur/slugify, делающая slug как из английских слов, так и из русских


return $this->render('posts/index.html.twig', [
     'posts' => $posts,
 ]);
!!!!!!именно 'posts', а не $posts, шаблонизатор Twig будете видеть в качестве переменной. !!!!!!!

{% %} делаем
{{ }} выводим

{% for post in posts %}
 {{ post.title }}
{% endfor %}

<p>{{ post.title }} </p>   можно так делать

npm run dev
npm run dev --watch

Проект работает на версии РНР 7.3
Cron:
php bin/console cronos:dump проверить конфиг крон бандла
crontab -e  создание кронтаба
crontab -l смотреть список табов
php bin/console cronos:replace перезаписать из аннотаций класса команды

====End Symphony====